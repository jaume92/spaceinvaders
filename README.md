# Space Invaders #

Projecte fet per l'assignatura de programació avançada l'any 2015.

### Per jugar ###

El codi ja està compilat. Per jugar només cal fer doble clic a *SInv.class*.

![Captura de pantalla 2016-11-06 a les 11.28.14.png](https://bitbucket.org/repo/npejpB/images/1671521353-Captura%20de%20pantalla%202016-11-06%20a%20les%2011.28.14.png)